import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import axios from 'axios';

import User from './routes/User';
import PageNotFound from './routes/PageNotFound';
import Login from './routes/Login';
import { StateContext } from './state/StateProvider';
import { createBrowserHistory } from 'history';
import Loggs from './routes/Loggs';
import Modal from 'react-modal';
import Stomp from 'stompjs';
import Registration from './routes/Registration';
import Profile from './routes/Profile';
import AdminPage from './routes/AdminPage';
import ProductList from './routes/ProductList'

export class Routs extends React.Component {

    static contextType = StateContext;

    constructor(props){
        super(props);

        this.state = {
            mytoken: '',
            email: '',
            history: createBrowserHistory(),
            modalOpen: false,
            hevMessage: false,
            modalMessage: '',
            data: [],
            isAdmin: false,
            adminModalOpen: false,
            adminHevMessage: false,
            adminModalMessage: '',
            adminData: [],

        };

        this.myModalClose = this.myModalClose.bind(this);
        this.adminMyModalClose = this.adminMyModalClose.bind(this);
        this.adminMyModalRefuse = this.adminMyModalRefuse.bind(this);
        this.changeToken = this.changeToken.bind(this);
    }

    changeToken = (data) => {
        this.setState({
            mytoken : data,
        });
      } 

    changeEmail = (email, isAdmin) =>{
        this.setState({
            email: email,
            isAdmin: isAdmin === 1,
        });
        setTimeout(() => {
            this.connect();
        }, 1000);
    }

    connect() {
        if (this.state.email.length > 0) {
            this.client = Stomp.client(`ws://localhost:15674/ws`);
            this.client.connect('guest', 'guest', this.onConnect, console.error, '/');
        }
    }
    
    onConnect = () => {
        const headers = { ack: 'client-individual'};
        
        if (this.state.isAdmin) {
            this.adminSubscribe = this.client.subscribe(`/amq/queue/admin`, this.adminAddMessage, headers);
        } else{
            this.subscribe = this.client.subscribe(`/amq/queue/${this.state.email}`, this.addMessage, headers);
        }
      };

    addMessage = (message) => {
        const email = JSON.parse(message.body).email;
        if (email === this.state.email) {
            message.ack();
        } else {
            this.setState({
                data: [...this.state.data, message],
                modalOpen: true,
                });

            setTimeout( () => {
                this.message = this.state.data[0];

                this.setState({modalMessage: JSON.parse(this.message.body).message});
            },200);
        }
      };

    myModalClose(e) {
        if (this.state.data.length > 1){
            this.message.ack();
            this.message = this.state.data[1];
            this.setState({ 
                modalMessage: JSON.parse(this.message.body).message,
                data: this.state.data.slice(1) });
        } else {
            this.message.ack();
            this.setState({
                data: [],
                modalOpen: false,
            });
        }
    }

    adminAddMessage = (message) => {
        this.setState({
            adminData: [...this.state.adminData, message],
            adminModalOpen: true,
            });

        setTimeout( () => {
            this.adminMessage = this.state.adminData[0];

            this.setState({adminModalMessage: JSON.parse(this.adminMessage.body).message});
        },200);
        
    };

    allowRegistration(){
            const user= {
                userEmail: this.state.email,
                userPassword: this.state.password,
                userAddress: this.state.address,
                userAcount: this.state.acount,
            }
            const url = `http://localhost:8080/api/users`;  
            axios.post(url,user)
                .then(res => {
            });
    }

    adminMyModalClose(e) {
        if (this.state.adminData.length > 1){
            const queue = JSON.parse(this.adminMessage.body).user;
            const user= {
                userEmail: queue.userEmail,
                userPassword: queue.userPassword,
                userAddress: queue.userAddress,
                userAcount: queue.userAcount,
                email: this.state.email,
            };
            const url = `http://localhost:8080/api/users`;  
            axios.post(url,user, {
                headers: {
                'authorization' : this.state.mytoken,
                }
            })
                .then(res => {
                    this.adminMessage.ack();
                    this.adminMessage = this.state.adminData[1];
                    this.setState({ 
                        adminModalMessage: JSON.parse(this.adminMessage.body).message,
                        adminData: this.state.adminData.slice(1) });
            });
        } else {
            const queue = JSON.parse(this.adminMessage.body).user;
            const user= {
                userEmail: queue.userEmail,
                userPassword: queue.userPassword,
                userAddress: queue.userAddress,
                userAcount: queue.userAcount,
                email: this.state.email,
            };
            const url = `http://localhost:8080/api/users`;  
            axios.post(url,user, {
                headers: {
                'authorization' : this.state.mytoken,
                }
            })
                .then(res => {
                    this.adminMessage.ack();
                    this.setState({
                        adminData: [],
                        adminModalOpen: false,
                    });
                });
        }
    }

    adminMyModalRefuse(e){
        if (this.state.adminData.length > 1){
            this.adminMessage.ack();
            this.adminMessage = this.state.adminData[1];
            this.setState({ 
                adminModalMessage: JSON.parse(this.adminMessage.body).message,
                adminData: this.state.adminData.slice(1) });
        } else {
            this.adminMessage.ack();
            this.setState({
                adminData: [],
                adminModalOpen: false,
            });
        }
    }

    render(){
        return ( this.state.isAdmin ? 
                <>
                    <BrowserRouter history={this.state.history}>
                        <Switch>
                            <Route exact path="/users" render={ () => <User token={this.state.mytoken} email={this.state.email}/>}/>
                            <Route exact path='/adminpage' render={ () => <AdminPage token={this.state.mytoken} email={this.state.email}/>}/>
                            <Route exact path='/products' render={ () => <ProductList token={this.state.mytoken} email={this.state.email} isAdmin={this.state.isAdmin}/> }/>
                            <Route render={ (props) => <PageNotFound {...props}/>} />
                        </Switch>
                    </BrowserRouter>
                    <Modal
                        isOpen={this.state.adminModalOpen}
                        onRequestClose={this.adminMyModalClose}
                        ariaHideApp={false}
                    >
                        <h1 >Modal</h1>
                        <label>{this.state.adminModalMessage}</label>
                        <div className="form-row ">
                            <button className="btn btn-primary" onClick={this.adminMyModalClose}>Oke</button>
                            <button className="btn btn-primary" onClick={this.adminMyModalRefuse}>Refuse</button>
                        </div>
                    </Modal>
                </> :
                <>
                    <BrowserRouter history={this.state.history}>
                        <Switch>
                            <Route exact path="/" render={ () => <Login token={this.state.mytoken} changeToken={this.changeToken} changeEmail={this.changeEmail}/>}/>
                            <Route exact path="/login" render={ () => <Login token={this.state.mytoken} changeToken={this.changeToken} changeEmail={this.changeEmail}/>}/>
                            <Route exact path="/registration" render={ () => <Registration/>} />
                            <Route exact path="/myprofile" render={ () => <Profile token={this.state.mytoken} email={this.state.email}/>} />
                            <Route exact path='/products' render={ () => <ProductList token={this.state.mytoken} email={this.state.email}/>}/>
                            <Route exact path="/loggs" render={ () => <Loggs/>} />
                            <Route render={ (props) => <PageNotFound {...props}/>} />
                        </Switch>
                    </BrowserRouter>
                    <Modal
                        isOpen={this.state.modalOpen}
                        onRequestClose={this.myModalClose}
                        ariaHideApp={false}
                    >
                        <h1 >Modal</h1>
                        <label>{this.state.modalMessage}</label>
                        <div className="form-row ">
                            <button className="btn btn-primary" onClick={this.myModalClose}>Oke</button>
                        </div>
                    </Modal>
                </>
    )
        }    
}

export default Routs;