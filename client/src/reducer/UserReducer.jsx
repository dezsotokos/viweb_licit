const userReducer = (state, action) => {
    switch (action.type) {

      case 'tokenChanged':
        return {
          ...state,
          token: action.value,
        }
  
      default:
        return state;
    }
  };
  
  export default userReducer;
  