import React, { Component } from 'react';
import axios from 'axios'
import { StateContext } from '../state/StateProvider';
import { Link } from 'react-router-dom';



  export class Login extends Component {
    static contextType = StateContext;

    constructor(props){
        super(props);

        this.state = {
            email: '',
            password: '',
            link: "/products",
        }
    }

    fieldChange(field, event){
        const newState = Object.assign(this.state, {
            [field]: event.target.value
        });
        this.setState(newState);
    }

    onSubmit(){ 
        const url = 'http://localhost:8080/api/users/login'; 
        const auth = {
            userEmail: this.state.email,
            userPassword: this.state.password,
        } 
        axios.post(url,auth)
            .then(res => {
            if(res.data != null){
                this.props.changeToken(`Basic ${res.data.token}`);
                this.props.changeEmail(auth.userEmail, res.data.isAdmin);
            }
      })
      
    }

    closeModal(){
        this.setState({modalIsOpen:false});
    }

    render(){
            return(
                <form>
                    <div className="form-rowIn">
                    </div>
                    <div className="form-rowIn">
                        <div className="form-group col-md-4">
                            <label>Email</label>
                            <input type="email" 
                                className="form-control"  
                                id="inputEmail4" 
                                placeholder="Email"
                                value={this.state.email}
                                onChange={ (e) => this.fieldChange('email',e)}/>
                        </div>
                    </div>
                    <div className="form-rowIn">
                        <div className="form-group col-md-4">
                            <label >Password</label>
                            <input type="password" 
                                className="form-control" 
                                id="inputPassword4" 
                                placeholder="Password"
                                value={this.state.password}
                                onChange={ (e) => this.fieldChange('password',e)}/>
                        </div>
                    </div>
                    <div className="form-rowIn">
                    </div>
                    <div className="form-rowIn">
                        <div>
                            <button className="signinguest btn-primary" type="button" onClick={ () => this.onSubmit()}>
                                <Link to={this.state.link} className="dayoff-sidebar__link"  type="button" >
                                    Sign in
                                </Link>
                            </button>
                        </div>
                    </div>
                    <div className="form-rowIn">
                        <div>
                            <button className="signinguest btn-primary" type="button" onClick={ () => this.onSubmit()}>
                                <Link to="/registration" className="dayoff-sidebar__link"  type="button" >
                                    Registration
                                </Link>
                            </button>
                        </div>
                    </div>
                </form>
            )
    }
}

export default Login;
