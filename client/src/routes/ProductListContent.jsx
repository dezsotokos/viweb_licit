import React, { Component } from 'react';
import '../index.css';
import axios from 'axios';

class ProductListContent extends Component {

    constructor(props){
        super(props);

        this.state = {
            productGroup: '',
            productType: '',
            offer: '',
            price: '',
        }
    }


    componentDidMount(){
        setTimeout(() => {
            this.findProductGroup();
            this.findProductType();
            this.productBid();
        }, 500);
    }

    productBid(){
        const url = `http://localhost:8080/api/bids/${this.props.product.productId}`;
        axios.get(url, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                if (res.data.length !== 0){
                    this.setState({price: res.data.bidPeice});
                } else {
                    this.setState({price: this.props.product.productPrice});
                }
            }).catch((err) => console.log(err));
    }

    findProductGroup(){
        const url = `http://localhost:8080/api/productgroups/${this.props.product.productBidGroup}`;
        axios.get(url, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                this.setState({productGroup: res.data.productGroupName});
            }).catch((err) => console.log(err));
    }

    findProductType(){
        const url = `http://localhost:8080/api/producttypes/${this.props.product.productType}`;
        axios.get(url, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                this.setState({productType: res.data.productTypeName});
            }).catch((err) => console.log(err));
    }

    fieldChange(field, event){
            const newState2 = Object.assign(this.state, {
                [field]: event.target.value
            });
            this.setState(newState2);
    }

    // keressuk a termeket a licitalas kozt
    offer(){
        const url = `http://localhost:8080/api/bids/${this.props.product.productId}`;
        axios.get(url, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                if (res.data.length !== 0){
                    this.update(res.data);
                } else {
                    this.insert();
                }
            }).catch((err) => console.log(err));
    }

    // ha meg nem volt licitalva a termekre
    insert(){
        const bid ={
            bidUser: this.props.myId,
            bidProduct: this.props.product.productId,
            bidPeice: this.state.offer,
            email: this.props.email,
        };
        const url = `http://localhost:8080/api/bids`;
        axios.post(url,bid, {
            headers: {
                'authorization' : this.props.token,
            }
        })
            .then(res => {
                this.setState({price: this.state.offer});
                this.setState({offer: ''});
            }).catch((err) => console.log(err));
    }

    // ha mar licitalva volt a termekre, akkor modositjuk azt
    update(data){
        const bid ={
            bidId: data.bidId,
            bidProduct: data.bidProduct,
            bidUser: this.props.myId,
            bidPeice: this.state.offer,
            email: this.props.email,
        };
        const url = `http://localhost:8080/api/bids/` + data.bidId;
        axios.patch(url,bid, {
            headers: {
                'authorization' : this.props.token,
            }
        })
            .then(res => {
                this.setState({price: this.state.offer});
                this.setState({offer: ''});
            }).catch((err) => console.log(err));
    }

    
    
    render(){
        let userOrGuest = <>
            <td className="rowsGame" ></td>
            <td className="rowsGame" ></td>
            </>;
        if (this.props.email !== '' && new Date() < new Date(this.props.product.productAvaliableTime) && !this.props.isAdmin){
            userOrGuest = <> 
                <td className="rowsGame" >
                    <input type="text" 
                        className="form-control" 
                        id="inputId" 
                        placeholder="offer"
                        value={this.state.offer}
                        onChange={ (e) => this.fieldChange('offer',e)}/>
                </td>
                <td className="rowsGame" >
                    <div className="">
                        <button type="button" 
                                className="btn_ofer btn-primary"
                                onClick={ () => this.offer()}>
                                Add offer
                        </button>
                    </div>
                </td>
              </>;
        }
        return (
            <tr key={this.props.product.productId}>
                <td className="rowsGame" >{this.props.product.productName}</td>
                <td className="rowsGame" >{this.props.product.productDescription}</td>
                <td className="rowsGame" >{this.state.price} $</td>
                <td className="rowsGame" >{this.props.product.productAvaliableTime.split('T')[0]} {this.props.product.productAvaliableTime.split('T')[1].split('.')[0]}</td>
                <td className="rowsGame" >{this.state.productGroup}</td>
                <td className="rowsGame" >{this.state.productType}</td>
                {userOrGuest}
            </tr>
        )
    }
}

export default ProductListContent;


