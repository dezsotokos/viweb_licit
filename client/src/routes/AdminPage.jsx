import React, { Component } from 'react';
import '../index.css';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { StateContext } from '../state/StateProvider';

class Profile extends Component {

    static contextType = StateContext;
    constructor(props){
        super(props);

        this.state = {
            productGroupName: '',
            productGroupDescription: '',
            user: {},
            productGroups: [],
            selectedGroup: '',
            productTypes: [],
            selectedType: '',
            productName: '',
            productDescription: '',
            productPrice: '',
            productSell: false,
            productAvaliableTime: '',
        }
    }

    componentDidMount(){
        this.findAllProductGroup();
        this.findAllProductType();
    }

    // tipus es csoport lekerese az uj ermek beszurasahoz
    findAllProductGroup(){
        const url = `http://localhost:8080/api/productgroups`;
        axios.get(url, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                this.setState({
                    productGroups: res.data,
                    selectedGroup: res.data[0].productGroupId,
                });
            }).catch((err) => console.log(err));
    }

    findAllProductType(){
        const url = `http://localhost:8080/api/producttypes`;
        axios.get(url, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                this.setState({
                    productTypes: res.data,
                    selectedType: res.data[0].productTypeId,
                });
            }).catch((err) => console.log(err));
    }

    fieldChange(field, event){
            const newState = Object.assign(this.state, {
                [field]: event.target.value
            });
            this.setState(newState);
    }

    addProductGroup(){
        
        const productGroup ={
            productGroupName: this.state.productGroupName,
            productGroupDescription: this.state.productGroupDescription,
            email: this.props.email,
        };
        const url = `http://localhost:8080/api/productgroups`;
        axios.post(url,productGroup, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                const reset = this.state;
                reset.productGroupName = '';
                reset.productGroupDescription = '';
                this.setState(reset);
                this.findAllProductGroup();
            }).catch((err) => console.log(err));
    }

    addProduct() {
        
        const product = {
            productName: this.state.productName,
            productDescription: this.state.productDescription,
            productPrice: this.state.productPrice,
            productSell: this.state.productSell,
            productAvaliableTime: this.state.productAvaliableTime,
            productType: this.state.selectedType,
            productBidGroup: this.state.selectedGroup,
            email: this.props.email,
        }
        const url = `http://localhost:8080/api/products`;
        axios.post(url,product, {
            headers: {
            'authorization' : this.props.token,
            }
        })
        .then(res => {
            const reset = this.state;
            reset.productName = '';
            reset.productDescription = '';
            reset.productPrice = '';
            reset.productSell = '';
            reset.productAvaliableTime = '';
            this.setState(reset);
        }).catch((err) => console.log(err));
    
    }
    
    render(){
            return (
                    <form>
                        <div className="form-row ">
                            <div className="div_Button">
                                <button className="btn btn-primary" type="button" >
                                    <Link to="/products" className="dayoff-sidebar__link"  type="button" >
                                        Back
                                    </Link>
                                </button>
                            </div>
                        </div>
                        <div className="form-row ">
                            <div className="form-group col-md-2">
                                <label >Product group name</label>
                                <input type="text" 
                                    className="form-control" 
                                    id="inputId" 
                                    placeholder="Product group name"
                                    value={this.state.productGroupName}
                                    onChange={ (e) => this.fieldChange('productGroupName',e)}/>
                            </div>
                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Product group description</label>
                                <input type="text" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder="Product group description"
                                    value={this.state.productGroupDescription}
                                    onChange={ (e) => this.fieldChange('productGroupDescription',e)}/>
                            </div>
                        </div>
                        <div className="form-row reverse">    
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.addProductGroup()}>
                                        Add Productgroup
                                </button>
                            </div>
                        </div>


                        <div className="form-row ">
                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Produc tName</label>
                                <input type="text" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder=" ProductName"
                                    value={this.state.productName}
                                    onChange={ (e) => this.fieldChange('productName',e)}/>
                            </div>
                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Product Description</label>
                                <input type="text" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder=" ProductDescription"
                                    value={this.state.productDescription}
                                    onChange={ (e) => this.fieldChange('productDescription',e)}/>
                            </div>
                        </div>
                        <div className="form-row ">
                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Product Price</label>
                                <input type="text" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder="ProductPrice"
                                    value={this.state.productPrice}
                                    onChange={ (e) => this.fieldChange('productPrice',e)}/>
                            </div>
                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Product AvaliableTime</label>
                                <input type="text" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder="ProductAvaliableTime"
                                    value={this.state.productAvaliableTime}
                                    onChange={ (e) => this.fieldChange('productAvaliableTime',e)}/>
                            </div>
                        </div>
                        <div className="form-row ">
                            <select id="user" className="form-group col-md-4" type="text" value={this.state.selectedGroup} onChange={(e) => this.fieldChange('selectedGroup',e)}>
                                {this.state.productGroups.map((productGroup)=><option key={productGroup.productGroupId} value={productGroup.productGroupId}>{productGroup.productGroupName} </option>)}
                            </select>
                            <select id="user" className="form-group col-md-4" type="text" value={this.state.selectedType} onChange={(e) => this.fieldChange('selectedType',e)}>
                                {this.state.productTypes.map((productType)=><option key={productType.productTypeId} value={productType.productTypeId}>{productType.productTypeName} </option>)}
                            </select>
                        </div>
                        <div className="form-row reverse">    
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.addProduct()}>
                                        Add product
                                </button>
                            </div>
                        </div>
                    </form>
            )}
}

export default Profile;


