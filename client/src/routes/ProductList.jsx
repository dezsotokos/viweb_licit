import React, { Component } from 'react';
import '../index.css';
import axios from 'axios';
import { Link } from 'react-router-dom';

import ProductListContent from './ProductListContent';

class User extends Component {

    constructor(props){
        super(props);

        this.state = {
            productName: '',
            productGroups: [],
            selectedGroup: '',
            productTypes: [],
            selectedType: '',
            products: [],
            myId: '',
        }
    }


    componentDidMount(){
        this.findall();
        this.findAllProductGroup();
        this.findAllProductType();

        setTimeout(() => {
            this.getMyProfil();
        }, 500);
    }

    getMyProfil(){
        const url = `http://localhost:8080/api/users?userEmail=${this.props.email}`;
        axios.get(url, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                this.setState({myId: res.data.userId});
            }).catch((err) => console.log(err));
    }

    // termek csoportok es tipusok a kereseshez
    findAllProductGroup(){
        const url = `http://localhost:8080/api/productgroups`;
        axios.get(url, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                this.setState({
                    productGroups: res.data,
                    selectedGroup: res.data[0].productGroupId,
                });
            }).catch((err) => console.log(err));
    }

    findAllProductType(){
        const url = `http://localhost:8080/api/producttypes`;
        axios.get(url, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                this.setState({
                    productTypes: res.data,
                    selectedType: res.data[0].productTypeId,
                });
            }).catch((err) => console.log(err));
    }

    fieldChange(field, event){
            const newState2 = Object.assign(this.state, {
                [field]: event.target.value
            });
            this.setState(newState2);
    }

    // lekeresek
    findall(){
        const url = `http://localhost:8080/api/products`;  
        axios.get(url)
            .then(res => {
                this.setState({products: res.data});
        }).catch((err) => {console.log(err)});
    }

    findbyname(){
        if (this.state.productName !== ''){
            const url = `http://localhost:8080/api/products?prodactName=` + this.state.productName;
            axios.get(url)
                .then(res => {
                    this.setState({products: [res.data]});
                }).catch((err) => console.log(err));
        } else {
            this.findall();
        }
    }

    findbygroup(){
        if (this.state.selectedGroup !== 'all'){
            const url = `http://localhost:8080/api/products/productgroups/` + this.state.selectedGroup;
            axios.get(url)
                .then(res => {
                    this.setState({products: res.data});
                }).catch((err) => console.log(err));
        } else {
            this.findall();
        } 
    }

    findbytype(){
        if (this.state.selectedType !== 'all'){
            const url = `http://localhost:8080/api/products/productType/` + this.state.selectedType;
            axios.get(url)
                .then(res => {
                    this.setState({products: res.data});
                }).catch((err) => console.log(err));
        } else {
            this.findall();
        }
    }

    render(){
            
            return (
                    <form>
                        <div className="form-row ">
                            { this.props.isAdmin ? 
                                <>
                                    <div className="div_Button">
                                        <button className="btn btn-primary" type="button" >
                                            <Link to="/users" className="dayoff-sidebar__link"  type="button" >
                                                User manage
                                            </Link>
                                        </button>
                                    </div>
                                    <div className="div_Button">
                                        <button className="btn btn-primary" type="button" >
                                            <Link to='/adminpage' className="dayoff-sidebar__link"  type="button" >
                                                Product manage
                                            </Link>
                                        </button>
                                    </div>
                                </> : this.props.email ?
                                    <>
                                    <div className="div_Button">
                                        <button className="btn btn-primary" type="button" >
                                            <Link to="/myprofile" className="dayoff-sidebar__link"  type="button" >
                                                My profil
                                            </Link>
                                        </button>
                                    </div>
                                    </>: <></>
                            }
                        </div>
                        <div className="form-row ">
                            <div className="form-group col-md-2">
                                <label >Product name</label>
                                <input type="text" 
                                    className="form-control" 
                                    id="inputId" 
                                    placeholder="ProductName"
                                    value={this.state.productName}
                                    onChange={ (e) => this.fieldChange('productName',e)}/>
                            </div>
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.findbyname()}>
                                        Find by name
                                </button>
                            </div>
                        </div>
                        <div className="form-row ">
                            <select id="user" className="form-group col-md-4" type="text" value={this.state.selectedGroup} onChange={(e) => this.fieldChange('selectedGroup',e)}>
                                {this.state.productGroups.map((productGroup)=><option key={productGroup.productGroupId} value={productGroup.productGroupId}>{productGroup.productGroupName} </option>)}
                            </select>
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.findbygroup()}>
                                        Find by group
                                </button>
                            </div>
                        </div>
                        <div className="form-row ">
                            <select id="user" className="form-group col-md-4" type="text" value={this.state.selectedType} onChange={(e) => this.fieldChange('selectedType',e)}>
                                {this.state.productTypes.map((productType)=><option key={productType.productTypeId} value={productType.productTypeId}>{productType.productTypeName} </option>)}
                            </select>
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.findbytype()}>
                                        Find by type
                                </button>
                            </div>
                        </div>
                        <div className="form-row ">
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.findall()}>
                                        Find all
                                </button>
                            </div>
                        </div>
                        <div className="form-row ">
                        </div>
                        <div className="form-row " id="critic" >
                            <table className="table table-sm">
                                <thead>
                                    <tr>
                                        <th className="tHeader" scope="col">Name</th>
                                        <th className="tHeader" scope="col">Description</th>
                                        <th className="tHeader" scope="col">Price</th>
                                        <th className="tHeader" scope="col">Avaliable</th>
                                        <th className="tHeader" scope="col">Group</th>
                                        <th className="tHeader" scope="col">Type</th>
                                        <th className="tHeader" scope="col">Offer</th>
                                        <th className="tHeader" scope="col">Send offer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.products.map((product) => {
                                        return <ProductListContent key={product.productId} product={product} email={this.props.email} token={this.props.token} myId={this.state.myId} isAdmin={this.props.isAdmin}/>
                                    })}
                                </tbody>
                            </table>

                        </div>

                    </form>
            )}
}

export default User;


