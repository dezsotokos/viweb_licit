import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import axios from 'axios'

class Registration extends Component {
    
    constructor(props){
        super(props);

        this.state = {
            email: '',
            password: '',
            address: '',
            acount: '',
        }
    }

    fieldChange(field, event){
        const newState2 = Object.assign(this.state, {
            [field]: event.target.value
        });
        this.setState(newState2);
    }

    // elkuldi a regisztracio kerelmet
    onSubmit(e){
        const user= {
            userEmail: this.state.email,
            userPassword: this.state.password,
            userAddress: this.state.address,
            userAcount: this.state.acount,
        }
        const url = `http://localhost:8080/api/users/registration`;  
        axios.post(url,user)
            .then(res => {
        });

        const reset = this.state;
        reset.email = '';
        reset.password = '';
        reset.address = '';
        reset.acount = '';
        this.setState(reset);

    }
    
    render(){
            return (
                    <form>
                        <div className="form-row ">
                        </div>
                        <div className="form-row ">
                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Email</label>
                                <input type="email" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder="Email"
                                    value={this.state.email}
                                    onChange={ (e) => this.fieldChange('email',e)}/>
                            </div>
                            <div className="form-group col-md-4">
                                <label >Password</label>
                                <input type="password" 
                                    className="form-control" 
                                    id="inputPassword4" 
                                    placeholder="Password"
                                    value={this.state.password}
                                    onChange={ (e) => this.fieldChange('password',e)}/>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group  col-md-4">
                                <label>Address</label>
                                <input type="text" 
                                    className="form-control" 
                                    id="inputAddress" 
                                    placeholder="st. Romania, ci. Cluj-Napoca, con. Cluj, streat M.Cogalniceanu, nr 3"
                                    value={this.state.address}
                                    onChange={ (e) => this.fieldChange('address',e)}/>
                            </div>
                            <div className="form-group  col-md-4">
                                <label>Acountnumber</label>
                                <input type="text" 
                                    className="form-control" 
                                    id="acountNumber" 
                                    placeholder="ex. 11122223"
                                    value={this.state.address2}
                                    onChange={ (e) => this.fieldChange('acount', e)}/>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ (e) => this.onSubmit(e)}>
                                        Registration
                                </button>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="div_Button">
                                <button className="btn btn-primary" type="button" >
                                    <Link to="/login" className="dayoff-sidebar__link"  type="button" >
                                        Login
                                    </Link>
                                </button>
                            </div>
                        </div>
                    </form>
            )
    }
}

export default Registration;