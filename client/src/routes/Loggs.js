import React from 'react';
import Stomp from 'stompjs';
import '../index.css'

class Loggs  extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            data: [],
            connected: <b>Status: not connected...</b>,
            logLevel: '*',
        }

    }

    componentDidMount(){
        this.connect();
    }

    connect() {
        this.client = Stomp.client(`ws://localhost:15674/ws`);
        this.client.connect('guest', 'guest', this.onConnect, console.error, '/');
    }
    
    onConnect = () => {
        this.setState({ connected: <b>Connected</b>});
        this.subscribeToLogs(this.state.logLevel);
      };

    

    addMessage = (message) => {
        const body = JSON.parse(message.body);
        this.setState({
            data: [...this.state.data, body],
            });
      };

    subscribeToLogs(logLevel) {

        if (this.subscription) {
            this.client.unsubscribe(this.subscription.id);
          }
        
          this.subscription = this.client.subscribe(`/topic/newProduct`, this.addMessage);
    }

    changeSelector = (e) => {
        this.setState({logLevel: e.target.value,});
        this.subscribeToLogs(e.target.value);
    }

    
    render() {
        const expressions = this.state.data.map((current) => {
            return (
                <tr className={current.logLevel} key={current.time}>
                    <td className="rowsGame" >{current.clientId}</td>
                    <td className="rowsGame" >{current.text}</td>
                </tr>
            
        )})
        return (
            <div className="container">
                <h1>New product</h1>
                
                <div id="messages">
                    <table className="table table-sm">
                        <thead>
                            <tr>
                                <th className="tHeader" scope="col">Email</th>
                                <th className="tHeader" scope="col">Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            {expressions}
                        </tbody>
                    </table>
                </div>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/.3.3/stomp.min.js"></script>
            </div>  
        );
        };
    }
    
    export default Loggs;