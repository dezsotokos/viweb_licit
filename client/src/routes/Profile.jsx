import React, { Component } from 'react';
import '../index.css';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { StateContext } from '../state/StateProvider';

class Profile extends Component {

    static contextType = StateContext;
    constructor(props){
        super(props);

        this.state = {
            address: '',
            acount: '',
            password: '',
            repassword: '',
            user: {},
        }
    }

    componentDidMount(){
        
        setTimeout(() => {
            this.getMyProfil();
        }, 500);
    }

    fieldChange(field, event){
            const newState = Object.assign(this.state, {
                [field]: event.target.value
            });
            this.setState(newState);
    }

    getMyProfil(){
        const url = `http://localhost:8080/api/users?userEmail=${this.props.email}`;
        axios.get(url, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                this.setState({user: res.data});
            }).catch((err) => console.log(err));
    }

    // profil adatok modositasa
    update(){
        
        const usertemp ={
            userId: this.state.user.userId,
            userAddress: this.state.address,
            userAcount: this.state.acount,
            email: this.props.email,
        };
        if (this.state.address === ''){
            usertemp.addres = this.state.user.userAddress;
        }
        if (this.state.acount === ''){
            usertemp.acount = this.state.user.userAcount;
        }
        const url = `http://localhost:8080/api/users/` + usertemp.userId;
        axios.patch(url,usertemp, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                const reset = this.state;
                reset.acount = '';
                reset.address = '';
                this.setState(reset);
                this.getMyProfil();
            }).catch((err) => console.log(err));
    }

    updatePassword() {
        
        if (this.state.password === this.state.repassword){
            const usertemp ={
                userId: this.state.user.userId,
                userPassword: this.state.password,
                email: this.props.email,
            };
            const url = `http://localhost:8080/api/users/password/${this.state.user.userId}`;
            axios.patch(url,usertemp, {
                headers: {
                'authorization' : this.props.token,
                }
            })
            .then(res => {
                const reset = this.state;
                reset.password = '';
                reset.repassword = '';
                this.setState(reset);
            }).catch((err) => console.log(err));
        } else {
            console.error('Not similar passwords');
        }
    }
    
    render(){
            return (
                    <form>
                        <div>
                            <div className="div_Button">
                                <button className="btn btn-primary" type="button" >
                                    <Link to="/products" className="dayoff-sidebar__link"  type="button" >
                                        Back
                                    </Link>
                                </button>
                            </div>
                        </div>
                        <label>My profile</label>
                        <h3>Email: {this.state.user.userEmail}<br/>
                            Address: {this.state.user.userAddress}<br/>
                            Acount: {this.state.user.userAcount}<br/>
                        </h3>
                        <div className="form-row ">
                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Address</label>
                                <input type="email" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder="Address"
                                    value={this.state.address}
                                    onChange={ (e) => this.fieldChange('address',e)}/>
                            </div>

                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Acountnumber</label>
                                <input type="email" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder="Acountnumber"
                                    value={this.state.acount}
                                    onChange={ (e) => this.fieldChange('acount',e)}/>
                            </div>
                        </div>
                        <div className="form-row reverse">    
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.update()}>
                                        Update my profile
                                </button>
                            </div>
                        </div>
                        <div className="form-row ">
                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Password</label>
                                <input type="password" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder=" New Password"
                                    value={this.state.password}
                                    onChange={ (e) => this.fieldChange('password',e)}/>
                            </div>
                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Password</label>
                                <input type="password" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder=" New Password"
                                    value={this.state.repassword}
                                    onChange={ (e) => this.fieldChange('repassword',e)}/>
                            </div>
                        </div>
                        <div className="form-row reverse">    
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.updatePassword()}>
                                        Update password
                                </button>
                            </div>
                        </div>
                    </form>
            )}
}

export default Profile;


