import React, { Component } from 'react';
import '../index.css';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { StateContext } from '../state/StateProvider';

class User extends Component {

    static contextType = StateContext;
    constructor(props){
        super(props);

        this.state = {
            address: '',
            acount: '',
            user: [],
            usersTypes: [],
        }
    }

    componentDidMount(){
        const urlUserType = `http://localhost:8080/api/users/usersTypes`;
        axios.get(urlUserType)
            .then( res => {
                this.setState({userTypes: res.data});
            }).catch((err) => {console.log(err)});
    }

    fieldChange(field, event){
            const newState2 = Object.assign(this.state, {
                [field]: event.target.value
            });
            this.setState(newState2);
    }

    findall(){
        const url = `http://localhost:8080/api/users`;  
        axios.get(url)
            .then(res => {
                const users = res.data;
                // eslint-disable-next-line
                users.map((current) => {
                    current.userType = this.state.userTypes[current.userType-1].userTypeName;
                });
                this.setState({
                    user: users,
                    address: '',
                    acount: '',
                    id: '',
                });
        }).catch((err) => {console.log(err)});
    }

    findbyid(){
        const url = `http://localhost:8080/api/users/` + this.state.id;
        axios.get(url)
            .then(res => {
                const users = [res.data];
                // eslint-disable-next-line
                users.map((current) => {
                    current.userType = this.state.userTypes[current.userType-1].userTypeName;
                });
                this.setState({
                    user: users,
                    address: '',
                    acount: '',
                    id: '',
                });
            }).catch((err) => console.log(err));
    }

    delete(){
        const url = `http://localhost:8080/api/users/` + this.state.id;
        axios.delete(url, {
            headers: {
                'authorization' : this.props.token,
                'email': this.props.email,
            }
        })
            .then(res => {
                const reset = this.state;
                reset.id = '';
                reset.address = '';
                reset.acount = '';
                this.setState(reset);
            }).catch((err) => console.log(err));
    }

    update(){
        const usertemp ={
            userId: this.state.id,
            userAddress: this.state.address,
            userAcount: this.state.acount,
            email: this.props.email,
        };
        const url = `http://localhost:8080/api/users/` + usertemp.userId;
        axios.patch(url,usertemp, {
            headers: {
            'authorization' : this.props.token,
            }
        })
            .then(res => {
                const reset = this.state;
                reset.email = '';
                reset.name = '';
                reset.id = '';
                this.setState(reset);
            }).catch((err) => console.log(err));
    }
    
    render(){
            const expressions = this.state.user.map((current, index) => {
                return (
                    <tr key={current.userId}>
                        <th className="rowsGame" scope="row">{index+1}</th>
                        <td className="rowsGame" >{current.userId}</td>
                        <td className="rowsGame" >{current.userEmail}</td>
                        <td className="rowsGame" >{current.userAddress}</td>
                        <td className="rowsGame" >{current.userAcount}</td>
                        <td className="rowsGame" >{current.userType}</td>
                    </tr>
                
            )})
            return (
                    <form>
                        <div className="form-row ">
                            <div className="div_Button">
                                <button className="btn btn-primary" type="button" >
                                    <Link to="/products" className="dayoff-sidebar__link"  type="button" >
                                        Back
                                    </Link>
                                </button>
                            </div>
                        </div>
                        <div className="form-row ">
                            <div className="form-group col-md-2">
                                <label >Id</label>
                                <input type="text" 
                                    className="form-control" 
                                    id="inputId" 
                                    placeholder="Id"
                                    value={this.state.id}
                                    onChange={ (e) => this.fieldChange('id',e)}/>
                            </div>
                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Address</label>
                                <input type="text" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder="Address"
                                    value={this.state.address}
                                    onChange={ (e) => this.fieldChange('address',e)}/>
                            </div>

                            <div className="form-group col-md-4">
                                <label className="primaryLabel">Acountnumber</label>
                                <input type="text" 
                                    className="form-control"  
                                    id="inputEmail4" 
                                    placeholder="Acountnumber"
                                    value={this.state.acount}
                                    onChange={ (e) => this.fieldChange('acount',e)}/>
                            </div>
                            
                        </div>

                        <div className="form-row reverse">
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.findbyid()}>
                                        Find by id
                                </button>
                            </div>
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.findall()}>
                                        Find all
                                </button>
                            </div>
                        </div>
                        <div className="form-row reverse">    
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.update()}>
                                        Update
                                </button>
                            </div>
                            <div className="div_Button">
                                <button type="button" 
                                        className="btn btn-primary"
                                        onClick={ () => this.delete()}>
                                        Delete
                                </button>
                            </div>
                        </div>
                        <div className="form-row ">
                        </div>
                        <div className="form-row ">
                            <table className="table table-sm">
                                <thead>
                                    <tr>
                                        <th className="tHeader" scope="col">Number</th>
                                        <th className="tHeader" scope="col">Id</th>
                                        <th className="tHeader" scope="col">Email</th>
                                        <th className="tHeader" scope="col">Address</th>
                                        <th className="tHeader" scope="col">Acountnumber</th>
                                        <th className="tHeader" scope="col">Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {expressions}
                                </tbody>
                            </table>
                        </div>
                    </form>
            )}
}

export default User;


