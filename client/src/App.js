import React from 'react';
import userReducer from './reducer/UserReducer';
import ReactDOM from 'react-dom';

import { StateProvider } from './state/StateProvider';
import './App.css';

import {Routs} from './Routs';

const App = () => {

  const initialState = {
        user: {
            token: 'alma'
        }
    }

  const mainReducer = ({
      user,
    }, action) => ({
      user: userReducer(user, action),
    });
    
  return (
    <div>
      <StateProvider initialState={initialState} reducer={mainReducer}>   
        <Routs/>
      </StateProvider>
    </div>
    );
  
}

if (document.getElementById('app')) {
    ReactDOM.render(
      <App />,
      // eslint-disable-next-line no-undef
      document.getElementById('app'),
    );
  }

export default App;
