# Real-time web applications project

npm install a gyökérben, a client és a server fájlokban
Szükséges egy mysql adatbázis viweb nevű adatbázissal root felhasználóval. A táblákat létrehozza és majd mi fel tudjuk tölteni őket.
npm start-tal a server-ben tudjuk elindítani a szerver alkalmazásunkat és szintén npm start-tal a kliens részét a client könyvtárból.
A böngészőben a 3000-es porton fut.
/loggin útvonalon tudunk bejelentkezni, csak bejelentkezett felhasználó tud crud műveleteket végezni
/users úttal a felhasználókkal kapcsolatos crud műveleteket tudunk végrehajtani a mezők kitöltésével és a megfelelő gombra való kattintással(a gombok nevei megfelelő nevvel vannak ellátva)
/films útvonal a filmekhez kapcsolódik hasonlóan, mint a felhasználóknál
/critics útvonal a kritikákhoz kapcsolódik hasonlóan, mint a felhasználóknál
Az utobbi három útvonalből tudunk navigálni a masik kettőre.
/loggs útvonalon a crud műveleteketet listázunk ki, akár személyre szabva, akár az összeset
Ha egy felhasználó hozzászólást ír az általunk kedvelt filmhez, akkor azt mi megkapjuk egy felugró modal formájában.
Az utobbi két ponthoz szükségünk van egy lokális rabbitmq szerverre, amely tartalmaz amqp és web-stomp pluginokat.
Kedvenceket a film oldalon tud törölni és hozzáadni egy felhasználó az film id-ja szerint.