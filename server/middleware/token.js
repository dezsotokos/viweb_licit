const jwt = require('jsonwebtoken');

const secret = '1c28d07215544bd1b24faccad6c14a04';

exports.auth = () => (req, res, next) => {
  const auth = req.headers.authorization;
  if (auth) {
    const token = auth.split(' ')[1];
    jwt.verify(token, secret, (error, payload) => {
      if (payload) {
        return next();
      }
      return res.status(401).send('You are not logged in properly');
    });
  } else {
    res.status(401).send('You are not logged in properly');
  }
};
