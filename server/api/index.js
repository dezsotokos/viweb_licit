const express = require('express'),
  morgan = require('morgan'),
  bodyParser = require('body-parser'),
  userRoutes = require('./modul/users'),
  productRoutes = require('./modul/product'),
  productGroupRoutes = require('./modul/productgroup'),
  productTypeRoutes = require('./modul/producttype'),
  bidRoutes = require('./modul/bid');

const router = express.Router();
router.use(morgan('tiny'));
router.use(bodyParser.json());

router.use('/users', userRoutes);
router.use('/bids', bidRoutes);
router.use('/products', productRoutes);
router.use('/productgroups', productGroupRoutes);
router.use('/producttypes', productTypeRoutes);

module.exports = router;
