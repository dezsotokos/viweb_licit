const express = require('express'),
  bidDao = require('../../db/bid'),
  userDao = require('../../db/users'),
  productDao = require('../../db/product'),
  tokenMiddleware = require('../../middleware/token'),
  connect = require('../connection/connect');

const router = express.Router();

router.get('/:productId', (req, res) => {
  const { productId } = req.params;
  bidDao.findBidByProduct(productId)
    .then((bid) => (res.json(bid)))
    .catch((err) => res.status(500).json({ message: `Error while finding bid  with productID ${productId}: ${err.message}` }));
});

router.post('/', tokenMiddleware.auth(), (req, res) => {
  bidDao.insertBid(req.body)
    .then((bid) => {
      res.status(201).json(bid);
    })
    .catch((err) => res.status(500).json({ message: `Error while creating bid: ${err.message}` }));
});

function udpdateBid(req, res, bidId, user) {
  return productDao.findProductById(req.body.bidProduct)
    .then((product) => bidDao.updateBid(bidId, req.body)
      .then((rows) => {
        if (rows) {
          const text = `You was ower bid in product ${product.productName}!`;
          connect.send(req.body.email, user.userEmail, text);
          res.sendStatus(204);
        } else {
          res.status(404).json({ message: `Bid with ID ${bidId} not found.` });
        }
      }).catch((err) => res.status(500).json({ message: `Error while updating user with ID ${bidId}: ${err.message}` })))
    .catch(() => res.status(404).json({ message: `Product with ID ${req.body.bid.bidProduct} not found.` }));
}

// tullicitalas
router.patch('/:bidId', tokenMiddleware.auth(), (req, res) => {
  const { bidId } = req.params;
  return bidDao.findBidById(bidId)
    .then((bid) => {
      if (Number(bid.bidPeice) < Number(req.body.bidPeice)) {
        return userDao.findUserById(bid.bidUser)
          .then((user) => {
            udpdateBid(req, res, bidId, user);
          }).catch((error) => res.status(404).json({ message: `User with ID ${bid.bidUser} not found. ${error}` }));
      }
      return res.status(500).json({ message: 'Error! Price is to low!' });
    }).catch(() => res.status(404).json({ message: `Bid with ID ${bidId} not found.` }));
});

module.exports = router;
