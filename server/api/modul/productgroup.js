const express = require('express'),
  productGroupDao = require('../../db/productGroup'),
  userDao = require('../../db/users'),
  tokenMiddleware = require('../../middleware/token'),
  connect = require('../connection/connect');

const router = express.Router();

router.get('/', (req, res) => {
  productGroupDao.findAllProductGroup()
    .then((productGroups) => res.json(productGroups))
    .catch((err) => res.status(500).json({ message: `Error while finding all productGroups: ${err.message}` }));
});

router.get('/:productGroupId', (req, res) => {
  const { productGroupId } = req.params;
  productGroupDao.findProductGroupById(productGroupId)
    .then((productGroup) => (productGroup ? res.json(productGroup) : res.status(404).json({ message: `Product group with ID ${productGroupId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding product group with ID ${productGroupId}: ${err.message}` }));
});

function sendToQueue(productGroup, email) {
  const text = `Admin ${email} create "${productGroup.productGroupName}" product group`;
  userDao.findAllUsers()
    .then((users) => users.forEach((user) => connect.send(email, user.userEmail, text)));
}

router.post('/', tokenMiddleware.auth(), (req, res) => {
  if (req.body.productGroupName === undefined || req.body.productGroupDescription === undefined) {
    return res.status(400).json({ message: 'Bad request body or parameter' });
  }
  return productGroupDao.insertProductGroup(req.body)
    .then((productGroup) => {
      sendToQueue(productGroup, req.body.email, res);
      res.status(201).json(productGroup);
    })
    .catch((err) => res.status(500).json({ message: `Error while creating critic: ${err.message}` }));
});

module.exports = router;
