const express = require('express'),
  jwt = require('jsonwebtoken'),
  bcrypt = require('bcrypt'),
  userDao = require('../../db/users'),
  userTypeDao = require('../../db/userType'),
  tokenMiddleware = require('../../middleware/token'),
  connect = require('../connection/connect');

const router = express.Router();
const secret = '1c28d07215544bd1b24faccad6c14a04';

router.get('/', (req, res) => {
  if (req.query.userEmail === undefined) {
    userDao.findAllUsers()
      .then((users) => res.json(users))
      .catch((err) => res.status(500).json({ message: `Error while finding all users: ${err.message}` }));
  } else {
    userDao.findUserByEmail(req.query.userEmail)
      .then((user) => {
        res.json(user);
      })
      .catch((err) => res.status(500).json({ message: `Error while finding all users with userName ${req.query.userName}: ${err.message}` }));
  }
});

// felhasznalo tipus
router.get('/usersTypes', (req, res) => {
  userTypeDao.findAllUserTypes()
    .then((users) => res.json(users))
    .catch((err) => res.status(500).json({ message: `Error while finding all usertypes: ${err.message}` }));
});


router.get('/:userId', (req, res) => {
  const { userId } = req.params;
  userDao.findUserById(userId)
    .then((user) => (user ? res.json(user) : res.status(404).json({ message: `User with ID ${userId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding user with ID ${userId}: ${err.message}` }));
});

router.post('/login', (req, res) => {
  const { userEmail, userPassword } = req.body;
  userDao.findUserByEmail(userEmail)
    .then((user) => {
      if (bcrypt.compareSync(userPassword, user.userPassword)) {
        const myresponse = {
          token: jwt.sign({ userEmail }, secret),
          isAdmin: user.userType,
        };
        res.send(myresponse);
      } else {
        res.status(401).send('Email/password combination incorrect.');
      }
    })
    .catch((err) => res.status(500).json({ message: `Error while finding user with userEmail ${userEmail}: ${err.message}` }));
});

router.post('/registration', (req, res) => {
  if (req.body.userPassword === undefined || req.body.userEmail === undefined) {
    return res.status(400).json({ message: 'Bad request body' });
  }

  return userDao.findUserByEmail(req.body.userEmail)
    .then((user) => {
      if (user === undefined) {
        connect.registrate(req.body);
        return res.status(201).json({ message: 'The request was sended to admin!' });
      }
      return res.status(500).json({ message: 'Email exist!' });
    });
});

router.post('/', tokenMiddleware.auth(), (req, res) => {
  if (req.body.userPassword === undefined || req.body.userEmail === undefined) {
    return res.status(400).json({ message: 'Bad request body' });
  }
  const insertuser = req.body;
  insertuser.userPassword = bcrypt.hashSync(req.body.userPassword, 10);
  return userDao.insertUser(insertuser)
    .then((user) => {
      res.status(201).location(`${req.fullUrl}/${user.userId}`).json(user);
    })
    .catch((err) => res.status(500).json({ message: `Error while creating user: ${err.message}` }));
});

router.delete('/:userId', tokenMiddleware.auth(), (req, res) => {
  const { userId } = req.params;
  return userDao.deleteUser(userId)
    .then((rows) => {
      if (rows) {
        const text = `Delete user with id: ${userId}`;
        connect.send(req.headers.email, text, 'delete');
        return res.sendStatus(204);
      }
      return res.status(404).json({ message: `User with ID ${userId} not found.` });
    })
    .catch((err) => res.status(500).json({ message: `Error while deleting user with ID ${userId}: ${err.message}` }));
});


router.patch('/:userId', tokenMiddleware.auth(), (req, res) => {
  const { userId } = req.params;
  if (userId === undefined) {
    return res.status(400).json({ message: 'Bad request body or parameter' });
  }
  return userDao.findUserById(userId)
    .then((user) => {
      if (user) {
        return userDao.updateUser(userId, req.body)
          .then((rows) => {
            if (rows) {
              if (user.userEmail !== req.body.email) {
                const text = `Admin ${req.body.email} modified your profile`;
                connect.send(req.body.email, user.userEmail, text);
              }
              res.sendStatus(204);
            } else {
              res.status(404).json({ message: `User with ID ${userId} not found.` });
            }
          })
          .catch((err) => res.status(500).json({ message: `Error while updating user with ID ${userId}: ${err.message}` }));
      }
      return res.status(404).json({ message: `User with elami ${req.body.email} not found.` });
    }).catch((err) => res.status(500).json({ message: `Error while find user with email ${req.body.email}: ${err.message}` }));
});

// jelszo valtas
router.patch('/password/:userId', tokenMiddleware.auth(), (req, res) => {
  const { userId } = req.params;
  if (userId === undefined) {
    return res.status(400).json({ message: 'Bad request body or parameter' });
  }
  return userDao.updateUserPassword(userId, bcrypt.hashSync(req.body.userPassword, 10))
    .then((rows) => {
      if (rows) {
        res.sendStatus(204);
      } else {
        res.status(404).json({ message: `User with ID ${userId} not found.` });
      }
    })
    .catch((err) => res.status(500).json({ message: `Error while updating critic with ID ${userId}: ${err.message}` }));
});

module.exports = router;
