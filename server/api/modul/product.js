const express = require('express'),
  productDao = require('../../db/product'),
  tokenMiddleware = require('../../middleware/token'),
  connect = require('../connection/connect');

const router = express.Router();

router.get('/', (req, res) => {
  if (req.query.prodactName === undefined) {
    productDao.findAllProducts()
      .then((users) => res.json(users))
      .catch((err) => res.status(500).json({ message: `Error while finding all products: ${err.message}` }));
  } else {
    productDao.findProductByName(req.query.prodactName)
      .then((product) => (product ? res.json(product) : res.status(404).json({ message: `product with name ${req.query.prodactName} not found.` })))
      .catch((err) => res.status(500).json({ message: `Error while finding all prodact with name ${req.query.userName}: ${err.message}` }));
  }
});

// csoport szerinti lekeres
router.get('/productgroups/:productGroup', (req, res) => {
  const { productGroup } = req.params;
  productDao.findProductsByPrdodactGroup(productGroup)
    .then((products) => (products ? res.json(products) : res.status(404).json({ message: `Products with group ${productGroup} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding all product: ${err.message}` }));
});

// tipus szerinti lekeres
router.get('/productType/:productType', (req, res) => {
  const { productType } = req.params;
  productDao.findProductsByPrdodactType(productType)
    .then((products) => (products ? res.json(products) : res.status(404).json({ message: `Products with type ${productType} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding all product: ${err.message}` }));
});

router.post('/', tokenMiddleware.auth(), (req, res) => {
  if (req.body.productName === undefined || req.body.productPrice === undefined
    || req.body.productAvaliableTime === undefined) {
    return res.status(400).json({ message: 'Bad request body or parameter' });
  }
  return productDao.insertProduct(req.body)
    .then((product) => {
      const text = `Product ${product.productName} was published! You can make a bid!`;
      connect.newProduct(req.body.email, text);
      res.status(201).json(product);
    })
    .catch((err) => res.status(500).json({ message: `Error while creating product: ${err.message}` }));
});

module.exports = router;
