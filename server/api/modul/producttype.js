const express = require('express'),
  productTypeDao = require('../../db/productType');

const router = express.Router();

router.get('/', (req, res) => {
  productTypeDao.findAllProductType()
    .then((productTypes) => res.json(productTypes))
    .catch((err) => res.status(500).json({ message: `Error while finding all products: ${err.message}` }));
});

router.get('/:productTypeId', (req, res) => {
  const { productTypeId } = req.params;
  productTypeDao.findProductTypeById(productTypeId)
    .then((productType) => (productType ? res.json(productType) : res.status(404).json({ message: `productType with ID ${productTypeId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding productType with ID ${productTypeId}: ${err.message}` }));
});

module.exports = router;
