const amqp = require('amqplib');

let connection = amqp.connect();

//  egy emailre kuldeni az uzenetet
exports.send = (email, queueEmail, texts) => {
  const message = JSON.stringify({
    email,
    message: texts,
  });
  connection.then(async (con) => {
    const channel =  await con.createChannel();
    await channel.assertQueue(queueEmail);

    channel.sendToQueue(queueEmail, Buffer.from(message));
    await channel.close();
  }).catch(() => {
    connection = amqp.connect();
    connection.then(async (con) => {
      const channel =  await con.createChannel();
      await channel.assertQueue(queueEmail);

      channel.sendToQueue(queueEmail, Buffer.from(message));
      await channel.close();
    }).catch((error) => console.log(error));
  });
};

//  adminnak kuldeni
exports.registrate = (user) => {
  const message = JSON.stringify({
    user,
    message: `Someone want to registrate with email: ${user.userEmail}`,
  });
  connection.then(async (con) => {
    const channel =  await con.createChannel();
    await channel.assertQueue('admin');

    channel.sendToQueue('admin', Buffer.from(message));
    await channel.close();
  }).catch(() => {
    connection = amqp.connect();
    connection.then(async (con) => {
      const channel =  await con.createChannel();
      await channel.assertQueue('admin');

      channel.sendToQueue('admin', Buffer.from(message));
      await channel.close();
    }).catch((error) => console.log(error));
  });
};

// uj beszurt termek kuldese
exports.newProduct = (email, texts) => {
  const message = JSON.stringify({
    clientId: email,
    text: texts,
  });
  connection.then(async (con) => {
    const channel =  await con.createChannel();
    channel.publish('amq.topic', 'newProduct', Buffer.from(message));

    await channel.close();
  }).catch(() => {
    connection = amqp.connect();
    connection.then(async (con) => {
      const channel =  await con.createChannel();
      channel.publish('amq.topic', 'newProduct', Buffer.from(message));

      await channel.close();
    }).catch((error) => console.log(error));
  });
};
