const execute = require('./config');

// táblázat létrehozása
execute(`
  CREATE TABLE IF NOT EXISTS users_types (
    userTypeId INT NOT NULL AUTO_INCREMENT,
    userTypeName VARCHAR(64),
    PRIMARY KEY (userTypeId)
  );
`);

exports.findUserTypeById = (userTypeId) => execute('SELECT * FROM users_types WHERE userTypeId=?', [userTypeId])
  .then((userType) => userType[0]);

exports.findAllUserTypes = () => execute('SELECT * FROM users_types');
