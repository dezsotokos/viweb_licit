const execute = require('./config');

execute(`
  CREATE TABLE IF NOT EXISTS product_types (
    productTypeId BIGINT NOT NULL AUTO_INCREMENT,
    productTypeName VARCHAR(64) UNIQUE,
    PRIMARY KEY (producttypeId)
  );
`);

exports.findAllProductType = () => execute('SELECT * FROM product_types');

exports.findProductTypeById = (productTypeId) => execute('SELECT * FROM product_types WHERE productTypeId=?', [productTypeId])
  .then((productType) => productType[0]);

exports.findProductTypeByName = (productTypeName) => execute('SELECT * FROM product_types WHERE productTypeName=?', [productTypeName])
  .then((productType) => productType[0]);

exports.insertProductType = (productType) => execute('INSERT INTO product_types VALUES (default, ?, ?)', [productType.productTypeName]).then((result) => ({
  productTypeId: result.insertId,
  productTypeName: productType.productTypeName,
}));
