const execute = require('./config');

// táblázat létrehozása
execute(`
  CREATE TABLE IF NOT EXISTS users (
    userId BIGINT NOT NULL AUTO_INCREMENT,
    userEmail VARCHAR(64) UNIQUE,
    userPassword VARCHAR(64) NOT NULL,
    userAddress VARCHAR(64),
    userAcount VARCHAR(19),
    userType INT NOT NULL,
    PRIMARY KEY (userId)
  );
`);

exports.findAllUsers = () => execute('SELECT * FROM users');

exports.findUserById = (userId) => execute('SELECT * FROM users WHERE userId=?', [userId])
  .then((users) => users[0]);

exports.findUserByEmail = (userEmail) => execute('SELECT * FROM users WHERE userEmail=?', [userEmail])
  .then((users) => users[0]);

exports.insertUser = (user) => execute('INSERT INTO users VALUES (default, ?, ?, ?, ?, 2)', [user.userEmail, user.userPassword, user.userAddress, user.userAcount]).then((result) => ({
  userId: result.insertId,
  userEmail: user.userEmail,
  userPassword: user.userPassword,
  userAddress: user.userAddress,
  userAcount: user.userAcount,
  userType: 2,
}));

exports.deleteUser = (userId) => execute('DELETE FROM users WHERE userId=?', [userId])
  .then((result) => result.affectedRows > 0);

exports.updateUserPassword = (userId, userPassword) =>  execute('UPDATE users SET userPassword=? WHERE userId=?', [userPassword, userId]).then((result) => result.affectedRows > 0);

exports.updateUser = (userId, user) =>  execute('UPDATE users SET userAddress=?, userAcount=? WHERE userId=?', [user.userAddress, user.userAcount, userId]).then((result) => result.affectedRows > 0);
