const execute = require('./config');

// táblázat létrehozása
execute(`
  CREATE TABLE IF NOT EXISTS product_groups (
    productGroupId BIGINT NOT NULL AUTO_INCREMENT,
    productGroupName VARCHAR(64) UNIQUE,
    productGroupDescription VARCHAR(64),
    PRIMARY KEY (productGroupId)
  );
`);

exports.findAllProductGroup = () => execute('SELECT * FROM product_groups');

exports.findProductGroupById = (productGroupId) => execute('SELECT * FROM product_groups WHERE productGroupId=?', [productGroupId])
  .then((productGroup) => productGroup[0]);

exports.findProductGroupByName = (productGroupName) => execute('SELECT * FROM product_groups WHERE productGroupName=?', [productGroupName])
  .then((productGroup) => productGroup[0]);

exports.insertProductGroup = (productGroup) => execute('INSERT INTO product_groups VALUES (default, ?, ?)', [productGroup.productGroupName, productGroup.productGroupDescription]).then((result) => ({
  productGroupId: result.insertId,
  productGroupName: productGroup.productGroupName,
  productGroupDescription: productGroup.productGroupDescription,
}));
