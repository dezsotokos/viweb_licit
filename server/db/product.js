const execute = require('./config');

// táblázat létrehozása
execute(`
  CREATE TABLE IF NOT EXISTS products (
    productId BIGINT NOT NULL AUTO_INCREMENT,
    productName VARCHAR(64) UNIQUE,
    productDescription VARCHAR(256),
    productPrice INT,
    productSell BOOLEAN,
    productAvaliableTime DATETIME,
    productType INT NOT NULL,
    productBidGroup INT NOT NULL,
    PRIMARY KEY (productId)
  );
`);

exports.findAllProducts = () => execute('SELECT * FROM products');

exports.findProductById = (productId) => execute('SELECT * FROM products WHERE productId=?', [productId])
  .then((products) => products[0]);

exports.findProductByName = (productName) => execute('SELECT * FROM products WHERE productName=?', [productName])
  .then((products) => products[0]);

exports.findProductsByPrdodactGroup = (productGroup) => execute('SELECT * FROM products WHERE productBidGroup=?', [productGroup]);

exports.findProductsByPrdodactType = (productType) => execute('SELECT * FROM products WHERE productType=?', [productType]);

exports.insertProduct = (product) => execute('INSERT INTO products VALUES (default, ?, ?, ?, ?, ?, ?, ?)', [product.productName, product.productDescription, product.productPrice, product.productSell, product.productAvaliableTime, product.productType, product.productBidGroup]).then((result) => ({
  productId: result.insertId,
  productName: product.productName,
  productDescription: product.productDescription,
  productPrice: product.productPrice,
  productSell: product.productSell,
  productAvaliableTime: product.productAvaliableTime,
  productType: product.productType,
  productBidGroup: product.productBidGroup,
}));
