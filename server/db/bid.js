const execute = require('./config');

// táblázat létrehozása
execute(`
  CREATE TABLE IF NOT EXISTS bids (
    bidId BIGINT NOT NULL AUTO_INCREMENT,
    bidUser BIGINT NOT NULL,
    bidProduct BIGINT NOT NULL,
    bidPeice INT NOT NULL,
    PRIMARY KEY (bidId)
  );
`);

exports.findAllBids = () => execute('SELECT * FROM bids');

exports.findBidById = (bidId) => execute('SELECT * FROM bids WHERE bidId=?', [bidId])
  .then((bids) => bids[0]);

exports.findBidByUser = (bidUser) => execute('SELECT * FROM bids WHERE bidUser=?', [bidUser]);

exports.findBidByProduct = (bidProduct) => execute('SELECT * FROM bids WHERE bidProduct=?', [bidProduct])
  .then((bids) => bids[0]);

exports.insertBid = (bid) => execute('INSERT INTO bids VALUES (default, ?, ?, ?)', [bid.bidUser, bid.bidProduct, bid.bidPeice]).then((result) => ({
  bidId: result.insertId,
  bidUser: bid.bidUser,
  bidProduct: bid.bidProduct,
  bidPeice: bid.bidPeice,
}));

exports.deleteBid = (bidId) => execute('DELETE FROM bids WHERE bidId=?', [bidId])
  .then((result) => result.affectedRows > 0);

exports.updateBid = (bidId, bid) =>  execute('UPDATE bids SET bidUser=?, bidProduct=?, bidPeice=? WHERE bidId=?', [bid.bidUser, bid.bidProduct, bid.bidPeice, bidId]).then((result) => result.affectedRows > 0);
