const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit: 10,
  multipleStatements: true,
  database: 'licit',
  host: 'localhost',
  port: 3308,
  user: 'root',
  password: '',
});

module.exports = (query, options = []) => new Promise((resolve, reject) => {
  pool.query(query, options, (error, results) => {
    if (error) {
      reject(new Error(`Error while running '${query}: ${error}'`));
    }
    resolve(results);
  });
});
