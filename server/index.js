const express = require('express'),
  cors = require('cors'),
  appRoutes = require('./api');

const app = express();

app.use(cors({
  methods: ['GET', 'POST', 'PATCH', 'DELETE', 'PUT', 'INSERT'],
}));

app.use('/api', appRoutes);

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });

module.exports = app;
